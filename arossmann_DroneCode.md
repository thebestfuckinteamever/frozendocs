# Project: Frozen Fist
## Drone Code

The project needs a few things worked on. I've created **three** groups to simplify the process. 

[This link will direct you to the graphic flow chart for the project.](www.lucidchart.com) 

###Group One: Drone Life
Group one is in charge of **drone startup, life cycle, and drone death**. 

This includes, but is not limited to:

* Initiation of the drone
* Estimating player location
* Drone speed both moving and turning
* Recording distance travelled
* Calculating distance to next path segment
* Smoothing out corners without getting stuck


###Group Two: Drone Processing
Group two will be taking on the tasks of **checking for enemies, making sure enemies are not teammates, and transitioning back to patrol mode**.

These processes include:

* Calculating distance to enemy
* Confirming enemy is not self or teammate
* Targeting closest player
* If target evades, go back to patrolling
* Find list of points to reach target
* If target is outside of the point, go to nearest point


###Group Three: Enemies
Group three will be responsible for the **attack sequence, checking if the enemy is dead, and rewards**.

* Calculate distance to target
* Allow shooting if angle and timer allow
* Separate shots by .02 to .08
* Check for target death
* Bonus for shooting non deadzone targets
* Bonus for shooting a non-paused player
* Work on loop for calculating shots when in combat 

##### Please email [Andrew Rossmann](andrew.rossmann@smail.rasmussen.edu) with any further questions. 