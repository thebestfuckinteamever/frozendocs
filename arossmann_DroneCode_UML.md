###Andrew Rossmann Module 04


##Actors
The **user / pilot**  
The **enemy**  
The **drone**

These are the primary actors, because these are the ones who access the use case directly and interact with each other.

##Supporting Actor
The **server**

The reason why the server is considered a supporting actor is that it doesn't access these use cases directly. It's a support system for providing bonuses and keeping a log of the drone's activities. 

##Use Cases
* 1. Startup
* 2. Attacking
* 3. Autopilot
* 4. Take Damage
* 5. Give Bonus
* 6. End Life Cycle

These 6 use cases are shared and used / accessed by both the actors and supporting actors. 

**Startup** is shared between both the **pilot** and the **server** because upon startup, the pilot uses it and the server logs this information. 

**Attacking** is shared between all the main actors, as the **pilot** can attack manually, the **enemy** can attack the **drone**, and the **drone** can attack the **enemy**.

**Autopilot** is only shared between the **drone** and the **server** because the server sees when to activate it and send the information to the **drone**.

**Take Damage** is used by the **enemy** and **drone** because neither the **server** nor the **pilot** can actually take damage.

**Give Bonus** is strictly used by the **server** and sent to the **drone** as a result of attacking non-deadzone and non-paused enemies.

**End Life Cycle** is shared between the **drone**, the **enemy**, and the **server**. The server is secondary like when autopilot is used, but both the **enemy** and **drone** can be destroyed and end their life cycle. 