# TO DO LIST
---------------------

### THESE SHOULD HAPPEN BEFORE THE NEXT RELEASE:

* 1. Name the version **Psyche**.

* 2. Notify all those listed in the end of the **README**.

* 3. Ship it.

#### THESE ARE LOWER PRIORITY

* Need to use *different, clearly disjoint* numbering system on **bzfs**.  perhaps just
  start from 0001 when 1.10 is released.  otherwise it gets unnecessarily bumped to
  match the client version.  it's a *separate* communication protocol version.

* Server should track last reported player and shot positions

* Improve server picked spawn locations

* Remove UDP client setting. code should figure that out now.

* Add other db server features
  - [DB Server Wiki](http://bzflag.org/wiki/DbServers)
  - player/password registration
  - open group registration. one nick is master for each group
  - [Karma System](http://bzflag.org/wiki/KarmaSystem)

* Try to build **bzadmin** on Win32 using **PDCurses** (it works with the X11
  version of PDCurses). It currently works with **stdin/stdout**.

* Add ability to capture a screenshot to a gamma-corrected png from
  inside the game.

* Rewrite **Mac OS X** display/window code to support app switching,
  resolution mode changes, screen captures, etc.

* Make it so keys can be bound to multiple actions, and provide a way
   to do global scoping on key actions

* StateDatabase should probably have min max values for eval values

* Split **-public <desc>** into **-public, -description <desc>**, and **-email <contact>**

* Support **gameinfo** requests as one time udp packets and include *all the
  same information* that bzfls tracks.  We should allow server info, player and
  score lists over udp from any udp address, not just a "connected" player.

* Make **bcast** traffic and **serverlist** traffic less blocking. (ie: same **select()** call)

* Listen all the time on **udp** for **bcast** game info replies when in *Find Servers*

* Would be nice if **LocalPlayer<->Robots** wouldn't relay **Packets
  (MsgPlayerUpdate)** through server. **bzflag** should be able to act
  as a relay for a local player and the server should know to
  only send one **MsgUpdate** to the master, who will relay to the
  connected players/bots. This will allow multiple players
  behind one slow net connection to play multiple players
  behind another slow connection. (for example) ie: -relay

* **bzflag -relay** should attempt to listen and reply on **udp**, resending server
  info for which ever server it is connected to.  Descriptions should begin
  with "relay:" in this case. **-solo <n>** should enable this behavior.

* Rework the **BZDB->eval()** caching to support expression
  dependencies - if a variable changes, all the expressions
  that use that variable should be updated as well, or at
  the least, flush the entire cache when a callback happens

* Make the *vertical rico solution* more elegant - get rid of
  BoxBuilding::getNormal and fix existing code to do z -
  getNormalRect & the like.

* Require an email contact name for *public* servers. Perhaps
  unpublished by default

* Create a new **MsgThrottle** message sent from client to server
  to inform server to throttle **MsgPlayerUpdates** from other clients
  to **X. X** is set in **bzflag.bzc**. Server uses **PlayerInfo.lastState** to
  batch send **PlayerUpdates** after throttle time has passed.
  *Clients* timestamp updates themselves, to catch out of order
  packets, but *server* restamps timestamps to get consistent
  times for all messages.

* Lag information should be appended to **MsgPlayerUpdate** packet
  by server, and use half in dead reckoning calculations

* Remove all DNS lookups from client when contacting a server IP
  supplied from the list server

* Add http **proxy support** for list servers

* Add http **connect proxy support** for game servers

* Allow /dev/dsp* on **Linux** to be selected someplace. command line,
  environment var, config file, who knows. ;-)

* Some **bzdb** values are stored elsewhere and written to **bzdb**
  only on exit. These should be stored in **bzdb** for the *entire*
  time

* Add caching to **bzdb** for **integer/float** values, so they don't
  have to be **atoi()**'ed or **eval()**'ed all the time. **isTrue** would
  also be a good one to cache

* Document headers with **JavaDoc**-style comments (for doxygen)

* Update **doc/protocol.txt** all descriptors to new format

* Support **gameinfo** requests as *one time* **udp** packets (*if* **udp**)

* **bzadmin** should build *without* GL headers installed fails with:
  ../../include/bzfgl.h:35: GL/gl.h: No such file or directory

* Build a **prototype(4)** for **bsd, solaris**

* Fix up **irix idb**

* *If* we stay with **tcp/udp** then use the *same* ports for the
   **udp** pipe as for the **tcp** pipe on *both client and server*

* Encapsulate stuff requiring platform **#ifdef's:
   networking API** into **libNet.a.
   fork/exec** (used in **menus.cxx**) into **libPlatform.a**
   file system stuff (dir delimiter, etc.)
   user name stuff

* Clean up libraries that could be reused by other games

* Move robots to *separate* binaries and allow *either the
   client or server* to exec them. Have a *server option*
   to keep n players in the game which auto spawns/kills
   enough bots to do it. Get rid of bot player type
   completely. bots should get all message types.

* Smarter robots

* Add type of shot (normal, gm, sw, etc) to killed message

* **Radio chat**:
   *allow players to communicate via low-quality audio.*
   already some support built into the server for this.

### THESE NEED TO WAIT FOR THE NEXT PROTOCOL BREAKAGE

* Move **-synctime** data to *different Msg packet*, or create new one.
  **MsgSetVar (ala 1.8)** would be a good one

* Flags should all be **nullflags** from client view until
  * a) you pick one up
  * b) you get the *identify* flag (send updates for all flags?)
 
* If player drops a flag and it stays, players have it's *real ID*.

* Shorten laser to like 1.25 normal shot
  (This is needed for larger maps so we can send sparse player updates.)

* Server should send less frequent updates for "*distant*" players

* Move team base packing into **WorldInfo (bzfs)**
  and add **height (z)**

* Remove **flipz** from **pyr net code** and replace with **neg height**.

* Remove **shoothrough/drivethrough** and replace with one flag
  meaning passable

* ipv6 support

* Consider using [ENet](http://enet.cubik.org/) for
   the network protocol

* Implement texture cache on client and have server offer
   new textures as part of the world.

* Allow **loadable meshes** as "tanks"

* Supply tank "**meshes**" from the server

* Allow client to *choose* a mesh from the server list

* Implement **one way cross server teleporters**

* Implement a visual "server list" game server

* **Pre-game waiting room**:
   provide a means for players to gather *before and after*
   a game.  Basic text chat facilities.  allows players
   to wait for enough players for a game and to discuss
   strategy, etc.  could build this into **bzfs** easily.



